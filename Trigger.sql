CREATE OR REPLACE FUNCTION demolition_trigger() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE player SET elo = player_elo(new.demolisher) WHERE player.id = new.demolisher;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER ranking_trigger AFTER INSERT ON demolition FOR EACH ROW EXECUTE PROCEDURE demolition_trigger();

CREATE OR REPLACE FUNCTION fifty_trigger() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE player SET elo = player_elo(new.winner) WHERE player.id = new.winner;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER ranking_trigger AFTER INSERT ON fifty FOR EACH ROW EXECUTE PROCEDURE fifty_trigger();

CREATE OR REPLACE FUNCTION goal_trigger() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE player SET elo = player_elo(new.scorer) WHERE player.id = new.scorer;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER ranking_trigger AFTER INSERT ON goal FOR EACH ROW EXECUTE PROCEDURE goal_trigger();

CREATE OR REPLACE FUNCTION save_trigger() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE player SET elo = player_elo(new.goalkeeper) WHERE player.id = new.goalkeeper;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER ranking_trigger AFTER INSERT ON save FOR EACH ROW EXECUTE PROCEDURE save_trigger();

CREATE OR REPLACE FUNCTION shoot_trigger() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE player SET elo = player_elo(new.shooter) WHERE player.id = new.shooter;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER ranking_trigger AFTER INSERT ON shoot FOR EACH ROW EXECUTE PROCEDURE shoot_trigger();
