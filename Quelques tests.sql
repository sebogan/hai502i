/**
  On essaye ici de récupérer le nombre de but au cours du tournois par équipe.
 */
SELECT t.name, COUNT(*) as goals
FROM team t, goal g, player p, member m
WHERE g.scorer = p.id AND m.id = p.id AND m.team = t.id
GROUP BY t.name
ORDER BY goals DESC;

/**
  On essaye ici de récupérer le nombre de but au cours du tournois par joueur.
 */
SELECT m.nickname, COUNT(*) as goals
FROM goal g, player p, member m
WHERE g.scorer = p.id AND m.id = p.id
GROUP BY m.nickname
ORDER BY goals DESC;

/**
  On essaye ici de récupérer le nombre d'arrêts au cours du tournois par joueur.
 */
SELECT m.nickname, COUNT(*) as saves
FROM save s, player p, member m
WHERE s.goalkeeper = p.id AND m.id = p.id
GROUP BY m.nickname
ORDER BY saves DESC;

/**
  On essaye ici de récupérer le nombre de but au cours du tournois par équipe.
 */
SELECT t.name, COUNT(*)
FROM team t, goal g, player p, member m
WHERE g.scorer = p.id AND m.id = p.id AND m.team = t.id
GROUP BY t.name;

/**
  Les meilleurs joueurs du tournois.
 */
SELECT m.nickname, p.elo
FROM player p, member m
WHERE m.id = p.id
ORDER BY p.elo DESC
LIMIT 5;

/**
  Si on exécute plusieurs fois des insertions de but pour Atow, on voit que le elo est bien calculé
 */
CALL insert_goal(10,'2:45',7);
CALL insert_goal(10,'2:45',7);
CALL insert_goal(10,'2:45',7);
CALL insert_goal(10,'2:45',7);

/**
  Les meilleurs joueurs du tournois.
 */
SELECT m.nickname, p.elo FROM player p, member m WHERE m.id = p.id ORDER BY p.elo DESC LIMIT 5;

/**
  Sélectionne le ratio but/partie
 */
SELECT m.nickname,
       (SELECT count(*) FROM goal WHERE scorer = p.id) / cast((SELECT count(*) FROM game WHERE blue_team = m.team OR red_team = m.team) as double precision) as goal_avg
FROM player p, member m
WHERE p.id = m.id
ORDER BY goal_avg DESC;

/**
  Sélectionne le ratio but/tir pour chaque joueur
 */
SELECT m.nickname,
       cast((SELECT count(*) FROM shoot WHERE shooter = p.id) as double precision) / (SELECT count(*) FROM goal WHERE scorer = p.id) as goal_per_shoot_avg
FROM player p, member m
WHERE p.id = m.id AND (SELECT count(*) FROM goal WHERE scorer = p.id)  > 0
ORDER BY goal_per_shoot_avg DESC;
