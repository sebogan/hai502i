CREATE OR REPLACE FUNCTION player_elo(
    goal bigint,
    save bigint,
    shoot bigint,
    fifties bigint,
    demolition bigint
) RETURNS bigint AS $$
    BEGIN
        RETURN goal * 10 + save * 5 + shoot * 2 + fifties * 2 + demolition * 2;
    end;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION player_elo(
    pid bigint
) RETURNS bigint AS $$
    BEGIN
        RETURN player_elo(
            (SELECT COUNT(*) FROM goal WHERE goal.scorer = pid),
            (SELECT COUNT(*) FROM save WHERE save.goalkeeper = pid),
            (SELECT COUNT(*) FROM shoot WHERE shoot.shooter = pid),
            (SELECT COUNT(*) FROM fifty WHERE fifty.winner = pid),
            (SELECT COUNT(*) FROM demolition WHERE demolition.demolished = pid)
        );
    end;
$$ LANGUAGE plpgsql;
