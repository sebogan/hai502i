INSERT INTO team VALUES (DEFAULT,'Karmine Corp');
INSERT INTO team VALUES (DEFAULT,'Team Liquid');
INSERT INTO team VALUES (DEFAULT,'Oxygen');
INSERT INTO team VALUES (DEFAULT,'Quadrant');
INSERT INTO team VALUES (DEFAULT,'Moist Esports');
INSERT INTO team VALUES (DEFAULT,'Gen. G');
INSERT INTO team VALUES (DEFAULT,'Faze Clan');
INSERT INTO team VALUES (DEFAULT,'Version1');
INSERT INTO team VALUES (DEFAULT,'G2 Esports');
INSERT INTO team VALUES (DEFAULT,'Team Secret');

CALL insert_player('Amine','Benayachi','itachi','Karmine Corp');
CALL insert_player('Axel','Touret','vatira','Karmine Corp');
CALL insert_player('Brice','Bigeard','ExoTiiK','Karmine Corp');
CALL insert_coach('Wagner','Benjamin','Eversax','Karmine Corp');

CALL insert_player('Bruno Alexandre','Dos Santos Lopes','AcroniK','Team Liquid');
CALL insert_player('Oskar','Gozdowski','Oski','Team Liquid');
CALL insert_player('Tristan','Soyez','Atow','Team Liquid');
CALL insert_coach('André Filipe de Jesus','Ruivinho Costa','Xpére','Team Liquid');

CALL insert_player('Archie','Pickthall','archie','Oxygen');
CALL insert_player('Ole','Van Doorn','oaly','Oxygen');
CALL insert_player('Joris','Robben','Joreuz','Oxygen');
CALL insert_coach('Billy','Hinchliffe','billy','Oxygen');

CALL insert_player('Lucas','Rose','RelatingWave','Quadrant');
CALL insert_player('Dylan','Pickering','eekso','Quadrant');
CALL insert_player('Kurtis','Cannon','Kash','Quadrant');
CALL insert_coach('Nicolai','Vistesen Andersen','Snaski','Quadrant');

CALL insert_player('Joe','Young','Joyo','Moist Esports');
CALL insert_player('Finlay','Ferguson','rise','Moist Esports');
CALL insert_player('Maëllo','Ernst','AztraL','Moist Esports');
CALL insert_coach('Noah','Hinder','noah','Moist Esports');

CALL insert_player('Jack','Benton','ApparentlyJack','Gen. G');
CALL insert_player('Nick','Iwanski','Chronic','Gen. G');
CALL insert_player('Joseph','Kidd','noly','Gen. G');
CALL insert_coach('Braxton','Lagarec','Allushin','Gen. G');

CALL insert_player('Jason','Coral','Firstkiller','Faze Clan');
CALL insert_player('Caden','Pellegrin','Sypical','Faze Clan');
CALL insert_player('Nick','Costello','mist','Faze Clan');
CALL insert_coach('Raul','Diaz','Roll Dizz','Faze Clan');

CALL insert_player('Kyle','Storer','torment','Version1');
CALL insert_player('Robert','Kyser','comm','Version1');
CALL insert_player('Landon','Konerman','BeastMode','Version1');
CALL insert_coach('Jayson','Nunez','Fireburner','Version1');

CALL insert_player('Jacob','Knapman','JKnaps','G2 Esports');
CALL insert_player('Reed','Wilen','Chicago','G2 Esports');
CALL insert_player('Massimo','Franceschi','Atomic','G2 Esports');
CALL insert_coach('Matthew','Ackermann','Satthew','G2 Esports');

CALL insert_player('Roberto Lima','De Souza','Sad','Team Secret');
CALL insert_player('Olimpio','Torres','nxghtt','Team Secret');
CALL insert_player('Danilo','kv1','Michelini','Team Secret');
CALL insert_coach('Bruno','Roschel','BRUNOVISQUII','Team Secret');

INSERT INTO game VALUES (DEFAULT,8,4,'2022-12-8 13:00','2022-12-8 13:06');
INSERT INTO game VALUES (DEFAULT,3,9,'2022-12-8 14:00','2022-12-8 14:06');
INSERT INTO game VALUES (DEFAULT,7,5,'2022-12-9 16:15','2022-12-9 16:21');
INSERT INTO game VALUES (DEFAULT,2,10,'2022-12-8 15:10','2022-12-9 15:16');
INSERT INTO game VALUES (DEFAULT,6,3,'2022-12-8 17:20','2022-12-8 17:26');
INSERT INTO game VALUES (DEFAULT,2,8,'2022-12-8 18:05','2022-12-8 18:11');
INSERT INTO game VALUES (DEFAULT,4,10,'2022-12-8 18:40','2022-12-8 18:46');
INSERT INTO game VALUES (DEFAULT,1,5,'2022-12-9 13:45','2022-12-9 13:51');
INSERT INTO game VALUES (DEFAULT,3,8,'2022-12-9 13:00','2022-12-9 13:06');
INSERT INTO game VALUES (DEFAULT,2,7,'2022-12-9 15:40','2022-12-9 15:46');

/* 1) V1 VS QUADRANT */

CALL insert_shoot(1,'0:54',29);
CALL insert_goal(1,'0:54',29);
CALL insert_shoot(1,'1:34',15);
CALL insert_goal(1,'1:34',15);
CALL insert_shoot(1,'2:26',29);
CALL insert_save(1,'2:27',13);
CALL insert_shoot(1,'5:46',30);
CALL insert_goal(1,'5:46',30);
CALL insert_shoot(1,'5:58',14);
CALL insert_save(1,'5:59',31);
CALL insert_fifty(1,'0:02',13,30);
CALL insert_fifty(1,'0:58',31,13);
CALL insert_demolition(1,'4:43',14,29);
CALL insert_demolition(1,'3:13',31,13);

/* 2) OXYGEN VS G2 */

CALL insert_shoot(2,'0:32',9);
CALL insert_goal(2,'0:33',9);
CALL insert_shoot(2,'0:54',10);
CALL insert_goal(2,'0:54',10);
CALL insert_shoot(2,'1:21',34);
CALL insert_save(2,'1:23',11);
CALL insert_shoot(2,'1:42',11);
CALL insert_goal(2,'1:43',11);
CALL insert_shoot(2,'1:58',33);
CALL insert_save(2,'1:59',9);
CALL insert_shoot(2,'2:57',33);
CALL insert_goal(2,'2:58',33);
CALL insert_shoot(2,'3:45',35);
CALL insert_goal(2,'3:46',35);
CALL insert_fifty(2,'3:50',33,10);
CALL insert_fifty(2,'4:23',11,34);
CALL insert_demolition(2,'4:45',11,35);
CALL insert_demolition(2,'2:34',9,33);

/* 3) FAZE CLAN VS MOIST ESPORTS */

CALL insert_fifty(3,'0:05',27,19);
CALL insert_demolition(3,'0:46',19,26);
CALL insert_shoot(3,'0:54',25);
CALL insert_save(3,'0:55',17);
CALL insert_shoot(3,'1:35',25);
CALL insert_goal(3,'1:35',25);
CALL insert_shoot(3,'2:54',26);
CALL insert_save(3,'2:55',18);
CALL insert_shoot(3,'3:48',27);
CALL insert_goal(3,'3:48',27);

/* 4) TEAM LIQUID VS TEAM SECRET */

CALL insert_fifty(4,'0:06',5,38);
CALL insert_shoot(4,'0:59',7);
CALL insert_goal(4,'1:00',7);
CALL insert_shoot(4,'1:56',37);
CALL insert_save(4,'1:57',7);
CALL insert_demolition(4,'2:34',6,38);
CALL insert_shoot(4,'2:39',39);
CALL insert_save(4,'2:41',6);
CALL insert_shoot(4,'4:56',7);
CALL insert_goal(4,'4:56',7);

/* 5) GEN. G VS OXYGEN */

CALL insert_fifty(5,'0:04',23,11);
CALL insert_shoot(5,'3:56',23);
CALL insert_goal(5,'3:56',23);
CALL insert_demolition(5,'4:59',21,9);
CALL insert_shoot(5,'5:58',10);
CALL insert_save(5,'5:59',22);

/* 6) TEAM LIQUID VS VERSION 1 */

CALL insert_fifty(6,'0:03',7,31);
CALL insert_shoot(6,'3:54',7);
CALL insert_goal(6,'3:54',7);
CALL insert_shoot(6,'3:59',6);
CALL insert_save(6,'4:00',29);
CALL insert_shoot(6,'4:25',30);
CALL insert_goal(6,'4:26',30);
CALL insert_shoot(6,'5:47',31);
CALL insert_goal(6,'5:47',31);
CALL insert_fifty(6,'5:52',5,30);
CALL insert_demolition(6,'5:58',31,7);

/* 7) QUADRANT VS TEAM SECRET */

CALL insert_fifty(7,'0:02',15,39);
CALL insert_shoot(7,'2:56',13);
CALL insert_goal(7,'2:57',13);
CALL insert_shoot(7,'3:32',14);
CALL insert_goal(7,'3:32',14);
CALL insert_shoot(7,'4:51',15);
CALL insert_goal(7,'4:51',15);
CALL insert_demolition(7,'4:59',38,13);

/* 8) KC VS MOIST */

CALL insert_shoot(8,'2:50',1);
CALL insert_goal(8,'2:50',1);
CALL insert_shoot(8,'3:40',2);
CALL insert_goal(8,'3:40',2);
CALL insert_shoot(8,'4:57',1);
CALL insert_goal(8,'4:57',1);
CALL insert_shoot(8,'5:52',3);
CALL insert_goal(8,'5:52',3);
CALL insert_shoot(8,'2:07',17);
CALL insert_save(8,'2:08',2);
CALL insert_shoot(8,'3:20',18);
CALL insert_save(8,'3:23',2);
CALL insert_shoot(8,'4:17',19);
CALL insert_goal(8,'4:17',19);
CALL insert_fifty(8,'4:31',1,19);
CALL insert_demolition(8,'1:54',2,18);

/* 9) OXYGEN VS VERSION1 */

CALL insert_shoot(9,'1:32',11);
CALL insert_save(9,'1:33',31);
CALL insert_shoot(9,'2:45',9);
CALL insert_goal(9,'2:45',9);
CALL insert_shoot(9,'3:59',10);
CALL insert_goal(9,'4:00',10);
CALL insert_shoot(9,'5:30',30);
CALL insert_goal(9,'5:31',30);
CALL insert_shoot(9,'5:49',29);
CALL insert_save(9,'5:51',11);

/* 10) TEAM LIQUID VS FAZE CLAN */

CALL insert_fifty(10,'0:03',7,27);
CALL insert_shoot(10,'0:45',5);
CALL insert_goal(10,'0:45',5);
CALL insert_shoot(10,'1:35',7);
CALL insert_goal(10,'1:35',7);
CALL insert_shoot(10,'2:46',6);
CALL insert_goal(10,'2:46',6);
CALL insert_shoot(10,'4:52',5);
CALL insert_shoot(10,'4:52',5);
CALL insert_demolition(10,'5:53',25,7);